#include <iostream>
#include <iomanip>
#include <cmath>

class SomeClass
{
private:
	int SomeInt;
	double SomeDouble;
	bool SomeBool;
public:
	SomeClass() : SomeInt(0), SomeDouble(0), SomeBool(false)
	{}
	SomeClass(int _SomeInt, double _SomeDouble, bool _SomeBool) : SomeInt(_SomeInt), SomeDouble(_SomeDouble), SomeBool(_SomeBool)
	{}

	void ShowFields()
	{
		std::cout << "SomeInt = " << SomeInt;
		std::cout << ", SomeDouble = " << std::fixed << std::setprecision(2) << SomeDouble;
		std::cout << ", SomeBool = " << std::boolalpha << SomeBool << "\n";
	}
};

class Vector
{
private:
	double x, y, z;
public:
	Vector() : x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	double Length()
	{
		return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
	}
};

int main()
{
	SomeClass ClassExample1, ClassExample2(1, 2, true);
	ClassExample1.ShowFields();
	ClassExample2.ShowFields();

	Vector VectorExample(1, 2, 3);
	std::cout << "\nVector length is " << VectorExample.Length() << "\n";
	return 0;
}